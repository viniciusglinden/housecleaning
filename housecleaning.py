#!/usr/bin/env python3
'''!
@file
@author Vinícius Gabriel Linden
@date 2022-10-08
@brief Helps reviewing and removing unnecessary packages for arch based distributions.
'''

import pacman
from os import remove as rm, environ

FILE="/tmp/housecleaning"
'''
List of packages that should probably not be installed.
Key: package.
Value: Search for string (True), use exact string (False).
'''
DANGEROUS = {'-ucode': True,
             '66': True,
             'base': False,
             'bash': False,
             'connman': True,
             'dhcpcd': True,
             'efibootmgr': False,
             'elogind': True,
             'fakeroot': False,
             'filesystem': False,
             'fontconfig': False,
             'grub': False,
             'libxft-bgra': False,
             'linux': True,
             'networkmanager': True,
             'pacman': False,
             'runit': True,
             'sudo': False,
             'timeshift': False,
             'wpa_supplicant': False,
             'xorg-server': False,
             'xorg-xinit': False
             }

def message_user(message : str):
    print(f'\033[33m{message}\n'
    f'(press "y" to continue)\033[0m')
    if not "y" in input():
        print("Aborting at user request")
        exit(0)

def clear():
    print("\033c")

def setup() -> tuple[list[str], bool]:
    try:
        rm(FILE) # maybe unnecessary
    except:
        pass

    try:
        environ["SUDO_UID"]
    except:
        print("Run this script with root privileges\n\
Aborting")
        exit()

    packages = pacman.pacman("-Qqt")['stdout'][0:-1].split('\n')
    print('Do you wish to skip the packages that are considered dangerous for uninstall?\n\
(press "n" to consider uninstalling)')
    ignore = not "n" in input()
    return packages, ignore

def ask_package(package : str, ignore : bool) -> bool:
    info = pacman.get_info(package)
    if "as a dependency" in info['Install Reason']:
        return False

    warn = False
    try:
        DANGEROUS[package]
        warn = True
    except:
        for pkg in [key for key, value in DANGEROUS.items() if value]:
            if pkg in package:
                warn = True
    if ignore and warn:
        return False
    warn = "This package is considered dangerous and not recommended to uninstall!\n" \
            if warn else ""

    clear()
    print(f"{info['Name']}: {info['Description']}\n\
{warn}Uninstall?\n\
(press 'y' for to uninstall)")
    return 'y' in input()

def save(file : list[str]):
    with open(FILE, 'w') as f:
        for line in file:
            f.write(f"{line}\n")

def system_clean(packages : list[str]):
    clear()
    if len(packages) == 0:
        print("Nothing selected, just cleaning cache then")
    else:
        message_user(f"So, you want to remove the following packages:\n\
    {' '.join(packages)}\n\
    Correct?")
        pacman.remove(packages, purge=True)
    pacman.pacman("-Scc")

def main():
    clear()
    message_user("This script is very dangerous:\n\
If you remove a package that is necessary, you may not be able to restart your computer!\n\
We will tell you when the package most likely should not be uninstalled.\n\
Please keep a bootable USB key and a backup (e.g. timeshift) with you.")
    current, ignore = setup()
    remove = []
    for package in current:
        if ask_package(package, ignore):
            remove.append(package)
    save(remove)
    system_clean(remove)
    print("Done")

if __name__ == '__main__':
    main()
