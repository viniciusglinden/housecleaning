#!/usr/bin/env python3
'''!
@file
@author Vinícius Gabriel Linden
@date 2022-10-08
'''

from setuptools import setup

setup(
    name='housecleaning',
    version='0.1.0',
    author='Vinícius Gabriel Linden',
    author_email='linden@viniciuslinden.xyz',
    description='Helps reviewing and removing unnecessary packages.',
    install_requires=['python-pacman'],
    keywords=['pacman', 'arch', 'linux', 'archlinux', 'artix', 'artixlinux'],
    license='GPLv2'
)

