
# Housecleaning

Just a helper to clean the system when it is too full.

## TODOs

- Add option to read csv file to ignore packages according to DANGEROUS constant.
- Add OS recognition and set function pointers accordingly.
- Add PIP, Yarn, Node, etc.
- Add Qt user interface.
- Test if FILE exists and ask user if he wants to use it: try to find listed package first.
